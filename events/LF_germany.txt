# Germany Events

add_namespace = LF_germany

country_event = {
	id = LF_germany.1
	title = LF_germany.1.t
	desc = LF_germany.1.d
	picture = GFX_pabst_killed
	trigger = {
		date > 1936.1.23
		country_exists = GER
		TAG = GER
	}
	option = {
		name = LF_germany.1.a
	}
	mean_time_to_happen = {
		days = 1
	}
	fire_only_once = yes
}

