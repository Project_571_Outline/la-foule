add_namespace = LF_news

# Marquet Elected
news_event = {
	id = LF_news.1
	title = LF_news.1.t
	desc = LF_news.1.desc
	picture = GFX_Marquet_Elected
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = LF_news.1.a
		trigger = {
			original_tag = FRA
		}
		add_popularity = {
			ideology = neosocialism
			popularity = 0.1
		}
		add_political_power = 25
	}
	option = {
		name = LF_news.1.b
		trigger = {
			NOT = { original_tag = FRA }
			NOT = { has_government = neosocialism }
		}
	}
	option = {
		name = LF_news.1.c
		trigger = {
			NOT = { original_tag = FRA }
			has_government = neosocialism
		}
	}	
}

# Maurras Elected
news_event = {
	id = LF_news.2
	title = LF_news.2.t
	desc = LF_news.2.desc
	picture = GFX_Maurras_Elected
	
	major = yes
	
	is_triggered_only = yes
	
	option = {
		name = LF_news.2.a
		trigger = {
			original_tag = FRA
		}
		add_popularity = {
			ideology = neosocialism
			popularity = 0.1
		}
		add_political_power = 25
	}
	option = {
		name = LF_news.2.b
		trigger = {
			NOT = { original_tag = FRA }
			NOT = { has_government = neosocialism }
		}
	}
	option = {
		name = LF_news.2.c
		trigger = {
			NOT = { original_tag = FRA }
			has_government = neosocialism
		}
	}	
}