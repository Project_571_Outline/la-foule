defined_text = {
	name = GetPopLevel
	text = {
        trigger = {
            check_variable = { coalition_pop_total = 0 }
        }
        localization_key = party_pop_loc_key
    }
    text = {
          trigger = {
            check_variable = { coalition_pop_total > 0 }
        }
		localization_key = coalition_pop_loc_key
    }
}
