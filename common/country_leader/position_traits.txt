#####################
# Position traits
######################
# These have no effect and are just for events and to clarify ideology belongings
#####################

leader_traits = {

#####################
# Minister
#####################

	head_of_government = {}
	foreign_minister = {}
	economy_minister = {}
	interior_minister = {}
	defense_minister = {}

#####################
# Military Staff
#####################

	chief_of_staff = {}
	chief_of_army = {}
	chief_of_navy = {}
	chief_of_air_force = {}
	chief_of_intelligence = {}

#####################
# Political ideologies
#####################

	anarchism = {}
	marxism = {}
	liberal_marxism = {}
	impossibilism = {}
	social_democratism = {}
	possibilism = {}
	liberalism = {}
	conservatism = {}
	authoritarianism = {}
	despotism = {}
	radical_right = {}
	neosocialism = {}

}
