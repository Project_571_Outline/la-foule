scripted_gui = {

	Anarchism_Coalition = {
		window_name = "Anarchism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = anarchism_coalition
		}
	}
	Marxism_Coalition = {
		window_name = "Marxism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = marxism_coalition
		}
	}
	Liberal_Marxism_Coalition = {
		window_name = "Liberal_Marxism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = liberal_marxism_coalition
		}
	}
	Impossibilism_Coalition = {
		window_name = "Impossibilism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = impossibilism_coalition
		}
	}
	Social_Democratism_Coalition = {
		window_name = "Social_Democratism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = social_democratism_coalition
		}
	}
	Possibilism_Coalition = {
		window_name = "Possibilism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = possibilism_coalition
		}
	}
	Liberalism_Coalition = {
		window_name = "Liberalism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = liberalism_coalition
		}
	}
	Conservatism_Coalition = {
		window_name = "Conservatism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = conservatism_coalition
		}
	}
	Authoritarianism_Coalition = {
		window_name = "Authoritarianism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = authoritarianism_coalition
		}
	}
	Despotism_Coalition = {
		window_name = "Despotism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = despotism_coalition
		}
	}
	Radical_Right_Coalition = {
		window_name = "Radical_Right_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = radical_right_coalition
		}
	}
	Neosocialism_Coalition = {
		window_name = "Neosocialism_Coalition"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = neosocialism_coalition
		}
	}
		Anarchism_Banned = {
		window_name = "Anarchism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = anarchism_banned
		}
	}
	Marxism_Banned = {
		window_name = "Marxism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = marxism_banned
		}
	}
	Liberal_Marxism_Banned = {
		window_name = "Liberal_Marxism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = liberal_marxism_banned
		}
	}
	Impossibilism_Banned = {
		window_name = "Impossibilism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = impossibilism_banned
		}
	}
	Social_Democratism_Banned = {
		window_name = "Social_Democratism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = social_democratism_banned
		}
	}
	Possibilism_Banned = {
		window_name = "Possibilism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = possibilism_banned
		}
	}
	Liberalism_Banned = {
		window_name = "Liberalism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = liberalism_banned
		}
	}
	Conservatism_Banned = {
		window_name = "Conservatism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = conservatism_banned
		}
	}
	Authoritarianism_Banned = {
		window_name = "Authoritarianism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = authoritarianism_banned
		}
	}
	Despotism_Banned = {
		window_name = "Despotism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = despotism_banned
		}
	}
	Radical_Right_Banned = {
		window_name = "Radical_Right_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = radical_right_banned
		}
	}
	Neosocialism_Banned = {
		window_name = "Neosocialism_Banned"
		parent_window_token = politics_tab
		visible = {
			has_country_flag = neosocialism_banned
		}
	}
}