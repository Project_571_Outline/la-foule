﻿capital = 584

oob = "AUS_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 5

set_politics = {
	ruling_party = conservatism
	last_election = "1932.3.1"
	election_frequency = 48
	elections_allowed = yes
}
set_popularities = {
	conservatism = 100
}

#create_country_leader = {
#	
#	name = "Jonas Lote"
#	picture = "gfx//leaders//Africa//Portrait_Africa_Generic_2.dds"
#	expire = "1965.1.1"
#	ideology = centrism
#	traits = {
#		#
#	}
#}
