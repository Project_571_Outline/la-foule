﻿capital = 801

oob = "AUS_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 5

set_politics = {
	ruling_party = liberal_marxism
	last_election = "1933.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	marxism = 50
	liberal_marxism = 50
}

create_country_leader = {
	name = "Semyon Budyonny"
	desc = "POLITICS_MIKLOS_HORTHY_DESC"
	picture = "Portrait_Semyon_Budyonny.tga"
	expire = "1965.1.1"
	ideology = free_marxism_subtype
}
