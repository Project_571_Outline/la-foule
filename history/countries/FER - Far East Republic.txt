﻿capital = 563

oob = "AUS_1936"

set_research_slots = 3

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_engineers = 1
	tech_support = 1		
	tech_mountaineers = 1
	early_fighter = 1
}

set_convoys = 50

set_politics = {
	ruling_party = social_democratism
	last_election = "1932.11.9"
	election_frequency = 48
	elections_allowed = yes
}

set_popularities = {
	social_democratism = 40
	liberal_marxism = 30
	marxism = 20
	liberalism = 10
}

set_stability = 0.50

create_country_leader = {
	name = "Boris Shumyatsky"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "Portrait_Boris_Shumyatsky.tga"
	expire = "1965.1.1"
	ideology = social_democratism_subtype
	traits = {
		#
	}
}
