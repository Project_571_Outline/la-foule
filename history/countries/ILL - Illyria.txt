﻿capital = 109

oob = "CRO_1936"

set_research_slots = 3

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_engineers = 1
	tech_support = 1		
	tech_mountaineers = 1
	early_fighter = 1
}

set_convoys = 200

set_politics = {
	ruling_party = conservatism
	last_election = "1922.11.9"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	impossibilism = 0
	marxism = 0
	anarchism = 0
	liberal_marxism = 0
	social_democratism = 5
	possibilism = 35
	liberalism = 45
	conservatism = 10
	authoritarianism = 5
	despotism = 0
	radical_right = 0
	neosocialism = 0
}

set_stability = 0.50
create_country_leader = {
	name = "Vojislav Kecmanović"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = impossibilism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Josip Broz Tito"
	desc = ""
	picture = "Josip_Broz_Tito.dds"
	expire = "1965.1.1"
	ideology = marxism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Milan Gorkić"
	desc = ""
	picture = "Milan_Grol.dds"
	expire = "1965.1.1"
	ideology = anarchism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Vlado Šegrt"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = free_marxism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Đuro Đaković"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = social_democratism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Nikola Mandić"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = possibilism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Heinrich Lammasch"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = liberalism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Aleksandar Horvat"
	desc = ""
	picture = "Aleksandar Horvat.dds"
	expire = "1965.1.1"
	ideology = conservatism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Horthy Miklós"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "Miklos Horthy.dds"
	expire = "1965.1.1"
	ideology = despotism_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Horthy Miklós"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "Miklos Horthy.dds"
	expire = "1965.1.1"
	ideology = radical_right_subtype
	traits = {
		#
	}
}
create_country_leader = {
	name = "Ante Pavelić"
	desc = ""
	picture = ""
	expire = "1965.1.1"
	ideology = neosocialism_subtype
	traits = {
		#
	}
}
