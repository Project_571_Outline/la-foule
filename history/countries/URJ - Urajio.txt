﻿capital = 804

oob = "AUS_1936"

set_research_slots = 3

set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_recon = 1
	tech_engineers = 1
	tech_support = 1		
	tech_mountaineers = 1
	early_fighter = 1
}

set_convoys = 200

set_politics = {
	ruling_party = authoritarianism
	last_election = "1926.10.8"
	election_frequency = 48
	elections_allowed = no
}

set_popularities = {
	authoritarianism = 100
}

set_stability = 0.50

create_country_leader = {
	name = "Ōsumi Mineo"
	desc = "POLITICS_KURT_SCHUSCHNIGG_DESC"
	picture = "Portrait_Ōsumi_Mineo.tga"
	expire = "1965.1.1"
	ideology = authoritarianism_subtype
	traits = {
		#
	}
}
