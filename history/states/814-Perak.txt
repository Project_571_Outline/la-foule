
state={
	id=814
	name="STATE_814"
	manpower = 491100
	
	state_category = large_town
	
	resources={
		rubber=170 # was: 848
	}

	history={
		owner = MAL
		victory_points = {
			7329 1 
		}
		victory_points = {
			10297 1 
		}
		buildings = {
			infrastructure = 3
		}
		add_core_of = MAL
		add_claim_by = SST
	}

	provinces={
		1291 1364 1376 7342 4310 12144 12215 
	}
}
