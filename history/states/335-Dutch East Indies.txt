
state={
	id=335
	name="STATE_335" # West Java
	manpower = 12399439
	
	state_category = large_city
	
	resources={
		rubber=40 # was: 242
		oil=10 # was: 16
	}

	history={
		owner = HOL
		add_core_of = HOL
		add_core_of = INS
		victory_points = {
			7381 5
		}
		victory_points = {
			7421 3
		}
		buildings = {
			infrastructure = 7
			industrial_complex = 2
			arms_factory = 2
			dockyard = 2
			air_base = 3
			7381 = {
				naval_base = 6
			}
		}
	}

	provinces={
		1386 1398 1425 4406 4434 4621 7381 7421 10279 10307 12249
	}
}
