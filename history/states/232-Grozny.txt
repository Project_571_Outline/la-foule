
state={
	id=232
	name="STATE_232"
	manpower = 1658292
	
	state_category = rural
	
	resources={
		oil=19 # was: 26
	}

	history={
		owner = SOV
		buildings = {
			infrastructure = 4
		}
		
		add_core_of = SOV
		add_core_of = RUS
	}

	provinces={
		700 712 728 1450 3668 3682 3711 4463 6701 6730 6748 7554 7624 9645 
	}
}
