state= {
	id=817
	name="STATE_817"
	manpower = 827898
	resources={
		rubber=20 # was: 138
	}
	
	state_category = town

	history= {
		owner = HOL
		add_core_of = INS
		victory_points = {
			1316 1 
		}
		buildings = {
			infrastructure = 3
			1316 = {
				naval_base = 1
			}
		}
	}
	provinces={
		1230 1304 1316 2170 2222 4279 4409 4335 10282 12129 
	}
}
