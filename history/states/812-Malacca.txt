
state={
	id=812
	name="STATE_812"
	manpower = 87200
	
	state_category = large_town

	history={
		owner = SST
		victory_points = {
			13240 1
		}
		buildings = {
			infrastructure = 4
		}
		add_core_of = SST
	}

	provinces={
		13240
	}
}
