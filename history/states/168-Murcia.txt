state={
	id=168
	name="STATE_168"

	history={
		owner = SPR
		victory_points = {
			10024 3
		}
		buildings = {
			infrastructure = 5
			dockyard = 1
			air_base = 2
			10024 = {
				naval_base = 8

			}

		}
		add_core_of = SPR

	}

	provinces={
		10024 10109 1093 7111 9903 11884 4195 11807 942 11812 832 3835 
	}
	manpower=1007605
	buildings_max_level_factor=1.000
	state_category=town
}
