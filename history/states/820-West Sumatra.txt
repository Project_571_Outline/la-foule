state={
	id=820
	name="STATE_820"
	manpower = 2960410
	
	state_category = large_town
	
	resources={
		rubber=70 # was: 268
		aluminium=10 # was: 50
	}


	history=
	{
		owner = HOL
		add_core_of = INS
		victory_points = {
			10382 1
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
		}
	}
	provinces={
		1428 1460 4446 8360 10354 10382 10522 12126 	
	}
}
