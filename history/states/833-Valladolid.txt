state={
	id=833
	name="STATE_833"
	resources={
		tungsten=10.000 #added due to Portugal Setup
	}
	history={
		owner = SPR
		buildings = {
			infrastructure = 6
			industrial_complex = 1

		}
		add_core_of = SPR

	}

	provinces={
		903 9827 3820 11809 6936 9700
	}
	manpower=713628
	buildings_max_level_factor=1.000
	state_category=rural
}
