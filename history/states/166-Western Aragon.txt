state={
	id=166
	name="STATE_166"
	resources={
		steel=15.000
	}

	history={
		owner = SPR
		victory_points = {
			3816 3 
		}
		victory_points = {
			9840 1 
		}
		victory_points = {
			7213 1 
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			air_base = 3

		}
		add_core_of = SPR

	}

	provinces={
		3816 11737 6901 6878 813 948 7213 9812 9840 9842 11838
	}
	manpower=668365
	buildings_max_level_factor=1.000
	state_category=town
}
