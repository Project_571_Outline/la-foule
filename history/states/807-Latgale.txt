
state={
	id=807
	name="STATE_807"
	manpower = 80500	
	state_category = city
	
	history={
		owner = BAL
		victory_points = {
			310 2
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1			
			310 = {
				bunker = 2
			}
		}
		add_core_of = BAL
	}

	provinces={
		222 310 319 341 3298 6459 9275 
	}
}
