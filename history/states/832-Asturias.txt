state={
	id=832
	name="STATE_832"

	history={
		owner = SPR
		victory_points = {
			11707 3
		}
		buildings = {
			infrastructure = 6
			industrial_complex = 1
			arms_factory = 1
			11707 = {
				naval_base = 2

			}

		}
		add_core_of = SPR

	}

	provinces={
		3744 9719 11707 3729 9699 6749 9703 11688 11684
	}
	manpower=1197252
	buildings_max_level_factor=1.000
	state_category=large_town
}
