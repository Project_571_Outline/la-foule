
state={
	id=813
	name="STATE_813"
	manpower = 737600
	
	state_category = large_town
	
	resources={
		aluminium = 20 # was: 25
	}

	history={
		owner = MAL
		buildings = {
			infrastructure = 3
			industrial_complex = 1
		}
		victory_points = {
			10297 1 
		}
		add_core_of = MAL
		add_claim_by = SST
	}

	provinces={
		4367 4412 4424 7427 10297 10313 
	}
}
