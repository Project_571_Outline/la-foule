
state={
	id=235
	name="STATE_235"
	manpower = 716516
	
	state_category = rural
	resources={
		chromium=14 # was: 20
	}

	history={
		owner = RUS
		buildings = {
			infrastructure = 4
			air_base = 2
		}
		add_core_of = SOV
		add_core_of = RUS
		victory_points = {
			11696 3
		}
	}

	provinces={
		748 3748 6753 9688 9707 9722 9726 9742 11678 11694 11696 11727 
	}
}
