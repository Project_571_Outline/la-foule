state=
{
	id=808
	name="STATE_808"
	manpower = 39200
	resources={
		oil=18
	}
	
	state_category = town

	history=
	{
		owner = BRU
		victory_points = {
			7387 3
		}
		buildings = {
			infrastructure = 5
			industrial_complex = 1
			7387 = {
				naval_base = 2
			}
		}
		add_core_of = BRU
	}

	provinces={
		7387	
	}
}
