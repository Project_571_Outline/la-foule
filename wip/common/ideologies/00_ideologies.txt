
ideologies = {
	
	anarchism = {									#无政府主义
		types = {
			anarchism_subtype = {}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_ANARCHISM_1"
			"FACTION_NAME_ANARCHISM_2"
			"FACTION_NAME_ANARCHISM_3"
			"FACTION_NAME_ANARCHISM_4"
			"FACTION_NAME_ANARCHISM_5"
		}
		
		color = { 90 0 0 }
		war_impact_on_world_tension = 0.6
		faction_impact_on_world_tension = 0.5
		can_be_boosted = no
		can_collaborate = no
		ai_communist = yes 
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = yes
		}
		can_host_government_in_exile = yes
		modifiers = {
			puppet_cost_factor = -0.3
			generate_wargoal_tension = 0.40
			lend_lease_tension = 0.20
			send_volunteers_tension = 0.20
		}
		faction_modifiers = {
			faction_trade_opinion_factor = 0.30 #plus 30% trade opinion
		}
		ai_neutral = yes 
	}
	
	
	marxism = {										#主流马克思主义
		types = {
			marxism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_MARX_1"
			"FACTION_NAME_MARX_2"
			"FACTION_NAME_MARX_3"
			"FACTION_NAME_MARX_4"
			"FACTION_NAME_MARX_5"
		}
		color = { 151 17 17 }
		war_impact_on_world_tension = 0.8
		faction_impact_on_world_tension = 0.5
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = no
		}
		modifiers = {
			puppet_cost_factor = -0.7
			generate_wargoal_tension = 0
			justify_war_goal_when_in_major_war_time = -0.5
			take_states_cost_factor = -0.25
			annex_cost_factor = -0.5
		}
		faction_modifiers = {
		}
		ai_neutral = yes
	}
	
	liberal_marxism = {								#自由马克思主义
		types = {
			free_marxism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_MARX_1"
			"FACTION_NAME_MARX_2"
			"FACTION_NAME_MARX_3"
			"FACTION_NAME_MARX_4"
			"FACTION_NAME_MARX_5"
		}
		color = { 238 0 0 }
		war_impact_on_world_tension = 0.8
		faction_impact_on_world_tension = 0.5
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = no
		}
		modifiers = {
			puppet_cost_factor = -0.7
			generate_wargoal_tension = 0
			justify_war_goal_when_in_major_war_time = -0.5
			take_states_cost_factor = -0.25
			annex_cost_factor = -0.5
		}
		faction_modifiers = {
		}
		ai_neutral = yes
	}
	
	impossibilism  = {								#不可能主义
		types = {
			impossibilism_subtype = {}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_IM_1"
			"FACTION_NAME_IM_2"
			"FACTION_NAME_IM_3"
			"FACTION_NAME_IM_4"
			"FACTION_NAME_IM_5"
		}
		
		color = { 255 56 56 }
		war_impact_on_world_tension = 0.6
		faction_impact_on_world_tension = 0.5
		can_be_boosted = no
		can_collaborate = no
		ai_communist = yes 
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = yes
		}
		can_host_government_in_exile = yes
		modifiers = {
			puppet_cost_factor = -0.3
			generate_wargoal_tension = 0.40
			lend_lease_tension = 0.20
			send_volunteers_tension = 0.20
		}
		faction_modifiers = {
			faction_trade_opinion_factor = 0.30 #plus 30% trade opinion
		}
		ai_neutral = yes 
	}
	
	social_democratism = {							#社会民主主义
		types = {
			social_democratism_subtype = {} 		#在此处添加新的定义名可以实现定义子意识形态（也可以在子意识形态下定义套娃意识形态
			#liberal_socialism_subtype = {}			#诸如此处是社会民主主义和自由社会主义，此为例子
		}
		dynamic_faction_names = {					#此处为定义派系名称，请在localisation里面进行本地化
			"FACTION_NAME_SD_1"
			"FACTION_NAME_SD_2"
			"FACTION_NAME_SD_3"
			"FACTION_NAME_SD_4"
			"FACTION_NAME_SD_5"
			"FACTION_NAME_SD_6"
		}
		
		color = { 205 0 87 }						#饼图上所显示的颜色RGB数值，因为本文件采用的是我自己mod里面的颜色（主要是不清楚你们的需要），所以麻烦自己修改一下
		
		rules = {									#游戏内规则，具体可以参考
			can_force_government = yes				#可以强迫改变政府
			can_send_volunteers = yes				#可以派遣志愿军
			can_puppet = yes						#可以强迫附庸
			can_guarantee_other_ideologies = yes	#可以保障其他意识形态的国家独立
			can_lower_tension = no					#在签署和平协议的时候不能降低全球的紧张度
		}
		
		can_host_government_in_exile = yes			#可以接纳流亡政府
		can_be_boosted = no							#不能在其他国家/地区提高这种意识形态的支持度
		can_collaborate = no						#不能够被资助起义
		war_impact_on_world_tension = 0.3			#该数值从-1到1，衡量这个意识形态的国家的战争行为对世界紧张度的作用（在此处为30%）
		faction_impact_on_world_tension = 0.1		#该数值从-1到1，衡量这个意识形态的派系的战争行为对世界紧张度的作用（在此处为10%）
		
		modifiers = {
			generate_wargoal_tension = 0.5			#制造战争借口的全球紧张度限制
			join_faction_tension = 0.30				#加入派系造成的全球紧张度限制
			lend_lease_tension = 0.20				#签署租借法案造成的全球紧张度限制
			send_volunteers_tension = 0.25			#派遣志愿军造成的全球紧张度限制
			guarantee_tension = 0.25				#保障独立的全球紧张度限制限制
			take_states_cost_factor = 0.5			#在和平协议中割地的全球紧张度成本加成
			annex_cost_factor = 0.5					#吞并国家在和平协议中的全球紧张度成本加成
		}
		
		faction_modifiers = {						#派系加成
			faction_trade_opinion_factor = 0.50 	#plus 50% trade opinion
		}
		ai_neutral = yes 							#使用的是原版的中立AI
	}
	
	possibilism = {									#自由主义
		types = {
			possibilism_subtype = {}
		}		
		dynamic_faction_names = {
			"FACTION_NAME_LIB_1"
			"FACTION_NAME_LIB_2"
			"FACTION_NAME_LIB_3"
			"FACTION_NAME_LIB_4"
			"FACTION_NAME_LIB_5"
			"FACTION_NAME_LIB_6"
		}	
		color = { 255 66 110 }		
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = yes
			can_only_justify_war_on_threat_country = no
			can_lower_tension = no
		}		
		can_host_government_in_exile = yes		
		war_impact_on_world_tension = 0.3		# 
		faction_impact_on_world_tension = 0.1
		can_be_boosted = no
		can_collaborate = no
		modifiers = {
			generate_wargoal_tension = 0.5
			join_faction_tension = 0.40
			lend_lease_tension = 0.10
			send_volunteers_tension = 0.20
			guarantee_tension = 0.30
			puppet_cost_factor = -0.5
			take_states_cost_factor = 0.25
			annex_cost_factor = 0.5
		}		
		faction_modifiers = {
			faction_trade_opinion_factor = 0.70 #plus 70% trade opinion
		}
		ai_neutral = yes # uses the neutrality AI behaviour
	}	

	liberalism = {									#自由主义
		types = {
			liberalism_subtype = {}
		}		
		dynamic_faction_names = {
			"FACTION_NAME_LIB_1"
			"FACTION_NAME_LIB_2"
			"FACTION_NAME_LIB_3"
			"FACTION_NAME_LIB_4"
			"FACTION_NAME_LIB_5"
			"FACTION_NAME_LIB_6"
		}	
		color = { 200 160 0 }		
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = yes
			can_only_justify_war_on_threat_country = no
			can_lower_tension = no
		}		
		can_host_government_in_exile = yes		
		war_impact_on_world_tension = 0.3		# 
		faction_impact_on_world_tension = 0.1
		can_be_boosted = no
		can_collaborate = no
		modifiers = {
			generate_wargoal_tension = 0.5
			join_faction_tension = 0.40
			lend_lease_tension = 0.10
			send_volunteers_tension = 0.20
			guarantee_tension = 0.30
			puppet_cost_factor = -0.5
			take_states_cost_factor = 0.25
			annex_cost_factor = 0.5
		}		
		faction_modifiers = {
			faction_trade_opinion_factor = 0.70 #plus 70% trade opinion
		}
		ai_neutral = yes # uses the neutrality AI behaviour
	}	
	
	conservatism = {								#保守主义
		types = {
			conservatism_subtype = {}
		}

		dynamic_faction_names = {
			"FACTION_NAME_CONSERVATISM_1"
			"FACTION_NAME_CONSERVATISM_2"
			"FACTION_NAME_CONSERVATISM_3"
			"FACTION_NAME_CONSERVATISM_4"
			"FACTION_NAME_CONSERVATISM_5"
		}
		color = { 12 50 100 }	
		war_impact_on_world_tension = 0.3		
		faction_impact_on_world_tension = 0.2
		can_be_boosted = no
		can_collaborate = no
		can_host_government_in_exile = yes
		rules = {
			can_force_government = yes
			can_puppet = yes
			can_guarantee_other_ideologies = yes
			can_send_volunteers = yes
		}	
		modifiers = {
			generate_wargoal_tension = 0.5
			join_faction_tension = 0.2
			lend_lease_tension = 0.4
			send_volunteers_tension = 0.4
			guarantee_tension = 0.4
			take_states_cost_factor = -0.1
			annex_cost_factor = -0.1
		}	
		faction_modifiers = {
			faction_trade_opinion_factor = 0.60 
			#plus 60% trade opinion
		}
		ai_neutral = yes # 
	}

	authoritarianism = {							#威权主义
		types = {
			authoritarianism_subtype  = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_AUTHORITARIANISM_1"
			"FACTION_NAME_AUTHORITARIANISM_2"
			"FACTION_NAME_AUTHORITARIANISM_3"
			"FACTION_NAME_AUTHORITARIANISM_4"
			"FACTION_NAME_AUTHORITARIANISM_5"
		}
		color = { 104 104 104 }
		war_impact_on_world_tension = 0.4		#no major danger
		faction_impact_on_world_tension = 0.2
		can_be_boosted = no
		can_collaborate = no
		can_host_government_in_exile = yes
		rules = {
			can_force_government = no
			can_puppet = yes
			can_send_volunteers = yes
			can_guarantee_other_ideologies = yes
		}
		modifiers = {
			generate_wargoal_tension = 0.6
			join_faction_tension = 0.3
			lend_lease_tension = 0.4
			send_volunteers_tension = 0.5
			guarantee_tension = 0.6
			take_states_cost_factor = -0.15
			annex_cost_factor = -0.2
		}
		faction_modifiers = {
			faction_trade_opinion_factor = 0.30 
			#plus 30% trade opinion
		}
		ai_neutral = yes 
	}
	
	despotism = {									#专制主义
		types = {
			despotism_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_DESPOTISM_1"
			"FACTION_NAME_DESPOTISM_2"
			"FACTION_NAME_DESPOTISM_3"
			"FACTION_NAME_DESPOTISM_4"
			"FACTION_NAME_DESPOTISM_5"
		}
		color = { 175 175 175 }	
		war_impact_on_world_tension = 1.0			
		faction_impact_on_world_tension = 1.0
		can_be_boosted = no
		can_collaborate = no
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = no
		}	
		modifiers = {
			justify_war_goal_when_in_major_war_time = -0.70
			puppet_cost_factor = -0.5
			generate_wargoal_tension = 0
			take_states_cost_factor = -0.25
			annex_cost_factor = -0.5
		}
		ai_neutral = yes # uses the fascist AI behaviour
	}
	
	radical_right = {								#激进右翼
		types = {
			radical_right_subtype = {}
		}
		dynamic_faction_names = {
			"FACTION_NAME_RADICAL_RIGHT_1"
			"FACTION_NAME_RADICAL_RIGHT_2"
			"FACTION_NAME_RADICAL_RIGHT_3"
			"FACTION_NAME_RADICAL_RIGHT_4"
			"FACTION_NAME_RADICAL_RIGHT_5"
		}
		color = { 69 47 3 }	
		war_impact_on_world_tension = 1.0			
		faction_impact_on_world_tension = 1.0
		can_be_boosted = no
		can_collaborate = no
		rules = {
			can_force_government = yes
			can_send_volunteers = yes
			can_puppet = yes
			can_guarantee_other_ideologies = no
		}	
		modifiers = {
			justify_war_goal_when_in_major_war_time = -0.70
			puppet_cost_factor = -0.5
			generate_wargoal_tension = 0
			take_states_cost_factor = -0.25
			annex_cost_factor = -0.5
		}
		ai_neutral = yes # uses the fascist AI behaviour
	}
	
	neosocialism = {								#新社会主义
		types = {
			neosocialism_subtype = {}
		}
		
		dynamic_faction_names = {
			"FACTION_NAME_NEOSOCIALISM_1"
			"FACTION_NAME_NEOSOCIALISM_2"
			"FACTION_NAME_NEOSOCIALISM_3"
			"FACTION_NAME_NEOSOCIALISM_4"
			"FACTION_NAME_NEOSOCIALISM_5"
		}
		color = { 115 22 75 }
		war_impact_on_world_tension = 0.9			
		faction_impact_on_world_tension = 0.5
		can_be_boosted = no
		can_collaborate = no
		rules = {
			can_force_government = yes
			can_send_volunteers = no
			can_puppet = yes
			can_guarantee_other_ideologies = no
		}
		modifiers = {
			justify_war_goal_when_in_major_war_time = -0.9
			puppet_cost_factor = -0.1
			generate_wargoal_tension = 0.1
			take_states_cost_factor = -0.5
		}
		ai_neutral = yes
	}
	
	}
