﻿capital = 803

oob = "AUS_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
}

set_convoys = 5

set_politics = {
	ruling_party = authoritarianism
	last_election = "Elections never held"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	authoritarianism = 100
}


create_country_leader = {
	name = "Mustafa Shokay"
	desc = ""
	picture = "Mustafa Shokay.dds"
	expire = "1953.3.1"
	ideology = authoritarianism_subtype
	traits = {
		
	}
}

